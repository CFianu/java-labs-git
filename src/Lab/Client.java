package Lab;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class Client {

    private String firstname;
    private String lastname;
    private MembershipType membershipType;
    private int membershipPoints = 0;

    private List<Trade> trades = new ArrayList<>();


    public int getMembershipPoints() {
        return membershipPoints;
    }

    public void setMembershipPoints(int membershipPoints) {
        this.membershipPoints = membershipPoints;
        checkMembership();
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public MembershipType getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(MembershipType membershipType) {
        this.membershipType = membershipType;
    }

    public Client(String firstname, String lastname){
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public void checkMembership(){
        if(membershipPoints < 10){
            setMembershipType(new Bronze());
        }else if (membershipPoints >= 10 && membershipPoints < 20){
            setMembershipType(new Silver());
        }else{
            setMembershipType(new Gold());
        }
    }

    public void addTrade(Trade trade){
        if(!canTrade()) return;
        
        trades.add(trade);
        setMembershipPoints(membershipPoints++);
    }

    public boolean exceeds10000(){
        double sum = trades.stream().map(Trade::getPrice).reduce((double) 0, Double::sum);
        return sum > 10000;
    }

    public boolean canTrade(){
        if(membershipType == null) return true;
        else if (exceeds10000()) return false;
        return tradesIn24hours() < membershipType.maxTrades;
    }

    public int tradesIn24hours(){
        LocalDate todaysDate = LocalDate.now();
        List<LocalDate> todaysTrades = trades.stream().map(Trade::getCreatedAt).filter(date -> date==todaysDate).collect(Collectors.toList());
        return todaysTrades.size();
    }
}
