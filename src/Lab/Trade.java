package Lab;

import java.time.LocalDate;

public abstract class Trade {

    private int ID;
    private String symbol;
    private int quantity;
    private double price;

    final private LocalDate createdAt = LocalDate.now();

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public abstract int callDividend();

    //initial constructor
    public Trade(int tradeID, String tradeSymbol, int tradeQuant, double tradePrice){
        this.ID = tradeID;
        this.symbol = tradeSymbol;
        this.quantity = tradeQuant;
        this.price = tradePrice;
    }

    //setting trade without a price
    public Trade(int tradeID, String tradeSymbol, int tradeQuant){
        this.ID = tradeID;
        this.symbol = tradeSymbol;
        this.quantity = tradeQuant;

    }

    //toString Method to print Lab.Trade
    public String toString(){
        return "Lab.Trade ID:" + String.valueOf(this.ID) + "\n" + "Lab.Trade Symbol: " + this.symbol + "\n" + "Lab.Trade Quantity:" + String.valueOf(this.quantity) + "\n" + "Lab.Trade Price:" + String.valueOf(this.price);
    }

    public void setPrice(double newprice){
        if(newprice > 0) {
            this.price = newprice;
        }
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }
}


