package Lab;

import java.util.ArrayList;
import java.util.List;

public class Account {

    private int valueOfTrades;



    public Account(int value){
        this.valueOfTrades = value;
    }

    public int getValues(){
        return this.valueOfTrades;
    }

    public void setValues(int newvalue){
        this.valueOfTrades = newvalue;
    }

    public void addTrade(Trade trade){
        this.valueOfTrades += (trade.getPrice() * trade.getQuantity());
    }
}
