package Lab;

public class FundTrades extends Trade {

    private int percDiv;

    public void setPercDiv(int percDiv, Trade trade) {
        this.percDiv = (int) (percDiv * trade.getPrice());
    }

    public FundTrades(int tradeID, String tradeSymbol, int tradeQuant, double tradePrice) {
        super(tradeID, tradeSymbol, tradeQuant, tradePrice);
    }

    public FundTrades(int tradeID, String tradeSymbol, int tradeQuant) {
        super(tradeID, tradeSymbol, tradeQuant);
    }

    @Override
    public int callDividend() {

        return this.percDiv;
    }
}
