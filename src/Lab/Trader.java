package Lab;

public class Trader {

    private String name;
    private Account tradeacc;

    public Trader(String name, Account tradeacc){
        this.name = name;
        this.tradeacc = tradeacc;
    }

    public void addTrade(Trade trade){
        tradeacc.addTrade(trade);
    }

}
