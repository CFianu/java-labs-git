package Lab;

public class BondTrades extends Trade{

    private int fixedDiv;

    public void setFixedDiv(int fixedDiv) {
        this.fixedDiv = fixedDiv;
    }

    public BondTrades(int tradeID, String tradeSymbol, int tradeQuant, double tradePrice) {
        super(tradeID, tradeSymbol, tradeQuant, tradePrice);
    }

    public BondTrades(int tradeID, String tradeSymbol, int tradeQuant) {
        super(tradeID, tradeSymbol, tradeQuant);
    }

    @Override
    public int callDividend() {
        return this.fixedDiv;
    }
}
