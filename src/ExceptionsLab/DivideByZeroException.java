package ExceptionsLab;

public class DivideByZeroException extends Exception {

    public DivideByZeroException(String errMessage){
        super(errMessage);
    }

}
